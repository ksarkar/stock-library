import java.util.List;
import java.util.ArrayList;

class Stock {
	
private String company;
private String symbol;
private double market_price;
private List <Double> current_chart;

	public Stock( String name, String symbol ){
		this.company = name;
		this.symbol = symbol;
		this.current_chart = new ArrayList<Double>();
	}
	
	public Stock( String name, String symbol , double price ){
		this.company = name;
		this.symbol = symbol;
		this.current_chart = new ArrayList<Double>();
		this.setMarketPrice( price );
	}

	public void setMarketPrice( double price ){
		this.market_price = price;
		this.current_chart.add( price );
	}
	
	public double getMarketPrice(){
		return this.market_price;
	}

	public String getCompany(){
		return this.company;
	}
	
	public String getSymbol(){
		return this.symbol;
	}
	
	private void setCompany( String name ){ ; }
	private void setSymbol( String symbol ){ ; }
	
	public String toString( String DELIM ){
		String string_representation = this.company + DELIM + this.symbol + DELIM + this.market_price;
		return string_representation;
	}
	
	public String toString(){
		return this.toString(",");
	}
	
	public void destroy(){
		this.company = null;
		this.symbol = null;
		this.market_price = 0;
		this.current_chart = null;
	}
	
}
