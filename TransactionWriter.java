import java.io.*;

class TransactionWriter {
	
	private int output_type;
	private FileWriter file_writer;
	private PipedWriter fifo_writer;

	private int FILE_TYPE = 1;
	private int FIFO_TYPE = 2;
	private int STREAM_TYPE = 3;

	public TransactionWriter(String file) throws IOException {
		this.output_type = this.FILE_TYPE;
		this.file_writer = new FileWriter(file);
	}

	public TransactionWriter(PipedReader pr) throws IOException {
		this.output_type = this.FIFO_TYPE;
		this.fifo_writer = new PipedWriter(pr);
	}
	
	public TransactionWriter() throws IOException {
		this.output_type = this.STREAM_TYPE;
	}

	public void write(Transaction transaction) throws IOException {
		String DELIM = " ";
		String buffer = transaction.toString();
		if (this.output_type == this.FILE_TYPE) 
			this.file_writer.write(buffer);
		if (this.output_type == this.FIFO_TYPE)
			this.fifo_writer.write(buffer);
		if (this.output_type == this.STREAM_TYPE)
			System.out.println(buffer);
	}

	public void close() throws IOException {
		if (this.output_type == this.FILE_TYPE) 
			this.file_writer.close();
		if (this.output_type == this.FIFO_TYPE)
			this.fifo_writer.close();
	}
}
