class Transaction {
	private String buyer_ID;
	private String seller_ID;
	private String transaction_ID;
	private Timestamp transaction_time;
	private String stock_symbol;
	private double price;
	private int quantity;
	
	public Transaction(String buyer, String seller, String ID, Timestamp time, String symbol, double bid_value, int qty) {
		this.buyer_ID = buyer;
		this.seller_ID = seller;
		this.transaction_ID = ID;
		this.transaction_time = time;
		this.stock_symbol = symbol;
		this.price = bid_value;
		this.quantity = qty;
	}
	
	public Transaction(String str) {
		String[] str_array = str.split(" ");

                this.transaction_ID = str_array[0];
                this.buyer_ID = str_array[1];
                this.seller_ID = str_array[2];
                this.stock_symbol = str_array[3];
                this.price = Double.parseDouble(str_array[4]);
                this.quantity = Integer.parseInt(str_array[5]);
                this.transaction_time = new Timestamp(str_array[6], ":");
	}

	private void setBuyer_ID(String buyer) {}
	private void setSeller_ID(String seller) {}
	private void setTransaction_ID(String ID) {}
	private void setTransaction_time(Timestamp time) {}
	private void setStock_symbol(String symbol) {}
	private void setPrice(double bid_value) {}
	private void setQuantity(int qty) {}

	public String getBuyer_ID() {
		return this.buyer_ID;
	}

	public String getSeller_ID() {
		return this.seller_ID;
	}

	public String getTransaction_ID() {
		return this.transaction_ID;
	}

	public Timestamp getTransaction_time() {
		return this.transaction_time;
	}

	public String getStock_symbol() {
		return this.stock_symbol;
	}

	public double getPrice() {
		return this.price;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public String toString(String DELIM) {
		String ID = this.transaction_ID + DELIM + this.buyer_ID + DELIM + this.seller_ID; 
		String stockInfo = this.stock_symbol + DELIM + this.price + DELIM + this.quantity;
		String time = this.transaction_time.toString();	
		return ID + DELIM + stockInfo + DELIM + time;
	}

	public String toString() {
		String DELIM = " ";
		return this.toString(DELIM);
	}

}

