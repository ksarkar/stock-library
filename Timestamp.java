import java.util.Calendar;

class Timestamp {

	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private int second;
	private int millisecond;

	public Timestamp(){
		Calendar rightNow = Calendar.getInstance();
		this.year = rightNow.get(Calendar.YEAR);
		this.month = rightNow.get(Calendar.MONTH);
		this.day = rightNow.get(Calendar.DAY_OF_MONTH);
		this.hour = rightNow.get(Calendar.HOUR_OF_DAY);
		this.minute = rightNow.get(Calendar.MINUTE);
		this.second = rightNow.get(Calendar.SECOND);
		this.millisecond = rightNow.get(Calendar.MILLISECOND);
	}
	
	public Timestamp(int year, int month, int day, int hour, int minute){
		this(year, month, day, hour, minute, 0, 0);
	}
	
	
	public Timestamp(int year, int month, int day, int hour, int minute, int second){
		this(year, month, day, hour, minute, second, 0);
	}
	
	public Timestamp(int year, int month, int day, int hour, int minute, int second, int millisecond){
		this.year = year;
		this.month = month;
		this.day = day;
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		this.millisecond = millisecond;
	}
	
	
	public String toString(){
		return this.toString(":");
	}
	
	public Timestamp(int parts[]){
		this.fill(parts);
	}
	
	public Timestamp (String time , String DELIM){
		int parts[] = stringToIntArray(time, DELIM);
		this.fill(parts);
	}
	
	public void blank(){
		this.year = 0;
		this.month = 0;
		this.day = 0;
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
		this.millisecond = 0;
	}
	
	public int compare(Timestamp ts){
		if( this.year > ts.year ) return 1;
		else if ( this.month > ts.month ) return 1;
		else if ( this.day > ts.day ) return 1;
		else if ( this.hour > ts.hour ) return 1;
		else if ( this.minute > ts.minute ) return 1;
		else if ( this.second > ts.second ) return 1;
		else if ( this.millisecond > ts.millisecond ) return 1;
		else if ( this.equals(ts) ) return 0;
		else return -1;
	}
	
	public boolean equals(Timestamp ts){
		if( this.year != ts.year ) return false;
		else if ( this.month != ts.month ) return false;
		else if ( this.day != ts.day ) return false;
		else if ( this.hour != ts.hour ) return false;
		else if ( this.minute != ts.minute ) return false;
		else if ( this.second != ts.second ) return false;
		else if ( this.millisecond != ts.millisecond ) return false;
		return true;
	}
	
	public String toString(String DELIM){
		return ( this.year + DELIM 
				+ this.month + DELIM 
				+ this.day + DELIM 
				+ this.hour + DELIM
				+ this.minute + DELIM
				+ this.second + DELIM
				+ this.millisecond ); 
	}
	
	private void fill(int parts[]){
		int size = parts.length;
		if(size > 0) this.year = parts[0];
		if(size > 1) this.month = parts[1];
		if(size > 2) this.day = parts[2];
		if(size > 3) this.hour = parts[3];
		if(size > 4) this.minute = parts[4];
		if(size > 5) this.second = parts[5];
		if(size > 6) this.millisecond = parts[6];
	}
	
	private int[] stringToIntArray(String time, String DELIM){
		String parts[] = time.split(DELIM);
		int size = parts.length;
		int int_parts[] = new int[size];
		for(int i = 0; i < size; i++){
			int_parts[i] = Integer.parseInt(parts[i]);
		}
		return int_parts;
	}
}
