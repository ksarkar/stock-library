/**
* @author Kripasindhu Sarkar
*/
public class Bid {
	protected String customer_ID;
	protected String share_symbol;
	protected double price;
	protected int quantity;
	protected char type;
	protected Timestamp time;
	protected Timestamp expiry_time;

	//constructor
	/** Constructor of the class. Takes all the attributes as input and constructs an object with those values.
	 * @param cid  customer id of the bidder
	 * @param share_symbol share symbol of the share company
	 * @param price price placed by the bidder
	 * @param quantity price placed by the bidder
	 * @param type type of the bid, can be B or S denoting the buybid and sellbid 
	 * @param t the time at which the bid is placed 
	 * @param exptime the expary time the bidder sets
 	 */	
	public Bid(String cid, String share_symbol, double price, int quantity, char type, Timestamp t, Timestamp exptime)
	{
		this.customer_ID = cid;
		this.share_symbol = share_symbol;
		this.price = price;
		this.quantity = quantity;
		this.type = type;
		this.time = t;
		this.expiry_time = exptime;
	}
	
	//constructor
	/** Constructor of the class. Takes all the attributes except the time as input and constructs an object with those values with the CURRENT time 
	 * @param cid  customer id of the bidder
	 * @param share_symbol share symbol of the share company
	 * @param price price placed by the bidder
	 * @param quantity price placed by the bidder
	 * @param type type of the bid, can be B or S denoting the buybid and sellbid 
	 * @param exptime the expary time the bidder sets
 	 */	
	public Bid(String cid, String share_symbol, double price, int quantity, char type, Timestamp exptime)
	{
		this.customer_ID = cid;
		this.share_symbol = share_symbol;
		this.price = price;
		this.quantity = quantity;
		this.type = type;
		this.expiry_time = exptime;
	}
	
	/** Constructor of the class.  
	 * @param str Given a string str containing the bid information in the specified order, 
	 * this constructor will parse it and construct the object with the required value
 	 */	
	public Bid(String str)
	{
		String[] str_array = str.split(" ");

		this.customer_ID = str_array[0];
		this.share_symbol = str_array[1];
		this.price = Double.parseDouble(str_array[2]);
		this.quantity = Integer.parseInt(str_array[3]);
		this.type = str_array[4].charAt(0);
		this.time = new Timestamp();
		this.expiry_time = new Timestamp(str_array[5], ":");
	}
	
	/** Copy-Constructor of the class.  
	 * @param b the bid from which to copy
 	 */
	public Bid(Bid b) {
		this.customer_ID = b.customer_ID;
		this.share_symbol = b.share_symbol;
		this.price = b.price;
		this.quantity = b.quantity;
		this.type = b.type;
		this.time = b.time;
		this.expiry_time = b.expiry_time;
	}
	
	//getters
	public String getCustomerID() {
		return customer_ID;
	}
	public String getShareSymbol() {
		return share_symbol;
	}
	public double getPrice() {
		return price;
	}
	public int getQuantity() {
		return quantity;
	}
	public char getType() {
		return type;
	}
	public Timestamp getTime() {
		return time;
	}
	public Timestamp getExpiryTime() {
		return expiry_time;
	}

	//setters
	protected void setcustomerid(String cid) {
		this.customer_ID = cid;
	}
	protected void setShareSymbol(String ssym) {
		this.share_symbol = ssym;
	}
	protected void setPrice(double price) {
		this.price = price;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	protected void setType(char type) {
		this.type = type;
	}
	protected void setTime(Timestamp time) {
		this.time = time;
	}
	protected void setExpiryTime(Timestamp exp_time) {
		this.expiry_time = exp_time;
	}


	//miscellaneous
	/** Function to convert bid information to human readable string 
	 * @param delim the delimeter to be added between the attributes
	 * @return String the human readable string containing all the bid information
 	 */
	public String toString(String delim) {
		return this.customer_ID + delim + 
		this.share_symbol + delim + 
		this.price + delim + 
		this.quantity + delim + 
		this.type + delim + 
		this.time.toString() + delim + 
		this.expiry_time.toString();
	}
	
	/** Function to convert bid information to human readable string 
	 * @param delim the delimeter to be added between the attributes
	 * @return String the human readable string containing all the bid information
 	 */
	public String toString() {
		return this.toString(" ");
	}

	public void display() {
		System.out.println( this.toString() );
	}
}

