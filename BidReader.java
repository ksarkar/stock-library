import java.io.*;

public class BidReader {
	private BufferedReader reader;
	private int READER_TYPE;
	
	public static int FILE = 1;
	public static int PIPE = 2;
	public static int IN = 3; 
		
	public BidReader(String file, int READER_TYPE) throws IOException {
		this.READER_TYPE = READER_TYPE;
		if ( READER_TYPE == this.FILE ) {
			this.reader = new BufferedReader(new FileReader(file));	
		}			
	}
	
	public BidReader(PipedWriter pw, int READER_TYPE) throws IOException{
		this.READER_TYPE = READER_TYPE;
		if ( this.READER_TYPE == this.PIPE ) {
			this.reader = new BufferedReader(new PipedReader(pw));	
		}			
	}
	
	public BidReader(int READER_TYPE){
		this.READER_TYPE = READER_TYPE;
		if ( this.READER_TYPE == this.IN ) {
			this.reader = new BufferedReader(new InputStreamReader(System.in));	
		}			
	}
	
	public Bid readBid()throws IOException{
		String line = this.reader.readLine();	
		System.out.println(line);
		Bid bid = new Bid(line);
		return bid;				
	}
}
